import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
  selector: 'navbar-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  @ViewChild("navbarlist", { static: true }) navbarlist: any;

  public open = true;

  toggleNavBar() {
    const navbar = <MatDrawer> this.navbarlist;
    const main = document.getElementsByTagName('main')[0];

    navbar.toggle();
    if(navbar.opened){
      this.open = true;
      main.classList.add("main-open");
    } else {
      this.open = false;
      main.classList.remove("main-open");
    }
  }

}
