import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { MenuUserComponent } from './modules/menu-user/menu-user.component';
import { MenuListComponent } from './modules/menu-list/menu-list.component';
import { MenuNotificationComponent } from './modules/menu-actions/menu-notification/menu-notification.component';
import { MenuAccountComponent } from './modules/menu-actions/menu-account/menu-account.component';
import { MenuInboxComponent } from './modules/menu-actions/menu-inbox/menu-inbox.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuUserComponent,
    MenuListComponent,
    MenuNotificationComponent,
    MenuInboxComponent,
    MenuAccountComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
