import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';

import { NavBarService } from './../../service/navbar.service';
import { MenuList } from './../../models/menu-list.model';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MenuListComponent implements OnInit {

  menusLaterais$: Observable<MenuList[]> = new Observable<MenuList[]>();

  constructor(private navBarService: NavBarService) { }

  ngOnInit() {
    this.getMenu();
  }

  public getMenu(): void {
    this.menusLaterais$ =  this.navBarService.getMenuList();
  }

  public verifyRouter(link: string): boolean {
    let active = false;
    if (link.replace('/','') === window.location.pathname.replace('/', '')) {
      active = true;
    };
    return active;
  }

}
