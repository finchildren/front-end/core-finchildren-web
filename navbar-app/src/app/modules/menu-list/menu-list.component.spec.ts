/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MenuListComponent } from './menu-list.component';
import { Observable } from 'rxjs';
import { MenuList } from 'src/app/models/menu-list.model';

describe('MenuListComponent', () => {
  let componentList: MenuListComponent;
  let listCompFixture: ComponentFixture<MenuListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    listCompFixture = TestBed.createComponent(MenuListComponent);
    componentList = listCompFixture.componentInstance;
    listCompFixture.detectChanges();
  });

  it('Criação do MenuListComponent', () => {
    expect(componentList).toBeTruthy();
  });

  it('Inicialização da variavel menusLaterais$', () => {
    const menuLateral = componentList.menusLaterais$ = new Observable<MenuList[]>();
    expect(menuLateral).toBeDefined();
  });

  it('Get menusLaterais$', () => {
    const menuLateral = componentList.getMenu();
    expect(menuLateral).toBeTruthy();
  });
});
