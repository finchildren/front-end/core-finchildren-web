import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  let componentApp: AppComponent;
  let appCompFixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    appCompFixture = TestBed.createComponent(AppComponent);
    componentApp = appCompFixture.componentInstance;
    appCompFixture.detectChanges();
  });

  it('Criação do AppComponent', () => {
    expect(componentApp).toBeTruthy();
  });

});
