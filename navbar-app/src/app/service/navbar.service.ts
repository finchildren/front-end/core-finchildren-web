import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';

// Models
import { MenuList } from './../models/menu-list.model';

@Injectable({
  providedIn: 'root'
})
export class NavBarService {
  _url = environment.serverUrl;

  menuListSubject$: BehaviorSubject<MenuList[]> = new BehaviorSubject<MenuList[]>([]);
  carregado: boolean = false;

  constructor(private http: HttpClient) { }

  getMenuList(): Observable<MenuList[]> {
    if (!this.carregado) {
      console.log(this._url);
      this.http.get<MenuList[]>(this._url)
        .pipe(
          tap((menus) => console.log(menus))
        )
        .subscribe(this.menuListSubject$);
      this.carregado = true;
    }
    return this.menuListSubject$.asObservable();
  }
}
