export class MenuList {
  _id ?: string;
  titulo: string = '';
  link: string = '';
  ordem: string = '';
  icon_material: string = '';
  links_filhos: Array<MenuList> = [];
  habilitado: boolean = true;
  dtCriacao: string = '';
}
